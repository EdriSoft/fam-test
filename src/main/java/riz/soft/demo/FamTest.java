package riz.soft.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication
public class FamTest {

	public static void main(String[] args) {
		SpringApplication.run(FamTest.class, args);
	}

	@RequestMapping("/")
	@ResponseBody
	public String home() {
		return "Hello World!";
	}
}
